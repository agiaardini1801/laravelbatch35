// Nama: Agia Ardini



1. Membuat Database => create database myshop;



2.Membuat Tabel di Dalam Databases

 
- create table users(
    -> id int(10) primary key auto_increment,
    
-> name varchar(255),
    -> email varchar (255),
    
-> password varchar (255));

  

- create table categories(
    
-> id int(10) primary key auto_increment,
    
-> name varchar(255));
    
   

- create table items(
    
-> id int(10) primary key auto_increment,
    
-> name varchar(255),
    
-> description varchar(255),
   
-> price int(10),
    
-> stock int(10),
    
-> category_id int(10),
    
-> CONSTRAINT FOREIGN KEY (category_id) REFERENCES categories (id));

3. Memasukkan Data pada Table


- insert into users(name, email, password) values("John Doe", "john2doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

- insert into categories(name) values ("gadget"),("cloth"),("men"),("women"),("branded");

- insert into items(name, description,price,stock,category_id) values ("Sumsang B50", "Hape keren dari merek sumsang", 4000000, 100, 1), 
("Uniklooh","Baju keren dari brand ternama", 500000,50, 2), ("IMHO Watch", "Jam tangan anak yang jujur banget", 2000000, 10,1);



4. Mengambil Data

a. 
a. select id,name,  email from users;

b. -  select *from items where price > 1000000;
   
   -  select *from items where name like 'watch%';

c. select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;



5. Mengubah data pada table


update items set price=25000000 where id=6; 
