<?php

require('animal.php');
require('Frog.php');
require('Ape.php');

$object = new animal  ("Shaun");

echo "Name:  $object->name <br>";
echo "Legs:  $object->legs <br>";
echo "Cold Blooded: $object->cold_blooded <br><br>";

$object2 = new Frog("Buduk");

echo "Name:  $object2->name <br>";
echo "Legs:  $object2->legs <br>";
echo "Cold Blooded: $object2->cold_blooded <br>";
$object2 -> aksi();
echo "<br> <br>";

$object3 = new Ape("Kera Sakti");

echo "Name:  $object3->name <br>";
echo "Legs:  $object3->legs <br>";
echo "Cold Blooded: $object3->cold_blooded <br>";
$object3 -> aksi();
