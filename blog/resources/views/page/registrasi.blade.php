@extends('layout.master')

@section('judul')
Buat Account Baru!
@endsection
@section('content')
  <form action="/welcome" method="post">
  @csrf
  <h3> Sign Up Form</h3> 

  <p> First Name: </p>
  <td><input type="text" name="firstname"></td>

  <p> Last Name: </p>
  <td><input type="text" name="lastname"></td>

  <p> Gender: </p>
  <p><input type="radio" name="gender" value="male"/> Male </p>
  <p><input type="radio" name="gender" value="female"/> Female </p>
  <p><input type="radio" name="gender" value="other"/> Other</p>

  <p> Nationality: </p>
  <select name ="nationality">     
	<option> Indonesia </option>
	<option> Inggris </option>
	<option> Korea </option>
	<option> Jepang </option>
	<option> Swiss </option> 
</select>

<p> Language Spoken: </p>
<p><input type = "checkbox" name="indonesia" /> Bahasa Indonesia </p>
<p><input type = "checkbox" name="inggris" />English </p>
<p> <input type="checkbox" name ="Other"> Other</p>

<p > Bio: </p>
<td><textarea name="bio"></textarea></td>
<p><a href ="/welcome"><button style="background-color: darkslategrey;"> Sign Up</button></a></p>
</form>
@endsection