@extends('layout.master')
@section('judul')
    Halaman List Cast
@endsection
@section('content')

<a href="/cash/create" class="btn btn-primary btn-sm my-2">Menambah Cast</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">bio</th>
        <th scope="col">action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $skey=>$item)
            <tr>
                <td>{{$skey+1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                   
                    <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                    <input type="submit" class="btn btn-sm btn-danger" value="delete">
                    </form>
                </td>
            </tr>
        @empty
            
      <tr>
        <td>Data Kosong</td>
      </tr>
      
      @endforelse
    </tbody>
  </table>
  @endsection
